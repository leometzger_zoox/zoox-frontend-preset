module.exports = (api, { withMobile, withCharts, product }) => {
  api.render(
    {
      "./src/App.vue": "./template/src/App.vue",
      "./public/favicon.ico": "./template/public/favicon.ico",
      "./src/main.js": "./template/src/main.js",
      "./src/plugins/designSystem.js": "./template/src/plugins/designSystem.js",
      "./src/layouts/Default.vue": "./template/src/layouts/Default.vue",
      "./src/views/Home.vue": "./template/src/views/Home.vue",
      "./src/router/index.js": "./template/src/router/index.js",
      "./src/router/routes.js": "./template/src/router/routes.js",
      "./src/router/guards/index.js": "./template/src/router/guards/index.js",
    },
    { product, withCharts, withMobile }
  );

  api.extendPackage({
    name: product.toLowerCase().split(" ").join("-"),
    dependencies: {
      "@zoox-ui/components": "^1.5.0",
      vuetify: "^2.4.2",
      "vue-page-transition": "^0.2.2",
    },
  });

  if (withCharts) {
    api.extendPackage({
      dependencies: {
        "@zoox-ui/charts": "^1.1.0",
      },
    });

    api.render({
      "./src/views/Dashboard.vue": "./template/src/views/Dashboard.vue",
    });
  }
};
