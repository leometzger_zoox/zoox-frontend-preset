import App from "./App.vue";
import Vue from "vue";

import i18n from "./i18n";
import router from "./router";
import store from "./store";
import { vuetify } from "./plugins/designSystem";

function main() {
  Vue.config.productionTip = false;

  new Vue({
    router,
    store,
    i18n,
    vuetify,
    render: (h) => h(App),
  }).$mount("#app");
}

main();
