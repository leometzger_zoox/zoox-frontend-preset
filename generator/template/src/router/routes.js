import DefaultLayout from "../layouts/Default.vue";

export default [
  {
    path: "/",
    component: DefaultLayout,
    children: [
      {
        path: "/",
        name: "home",
        component: () => import("../views/Home.vue"),
      },
      <% if(withCharts) { %>
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: () => import('../views/Dashboard.vue')
        },
      <% } %>
    ],
  },
];
