import Vue from "vue";
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import ZooxComponents, { Theme, ZooxIcons } from "@zoox-ui/components";
import "@zoox-ui/components/dist/zoox-design-system.css";

import VuePageTransition from "vue-page-transition";

<% if(withCharts) { %>
import ZooxCharts from '@zoox-ui/charts'
<% } %>

Vue.use(Vuetify)
Vue.use(VuePageTransition);
Vue.use(ZooxComponents);

<% if(withCharts) { %>
Vue.use(ZooxCharts);
<% } %>

export const vuetify = new Vuetify({
  theme: {
    light: true,
    ...Theme,
  },
  icons: {
    values: ZooxIcons,
  },
});
