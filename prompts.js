module.exports = [
  {
    type: "input",
    name: "product",
    message: "Qual o nome do produto?",
  },
  {
    type: "confirm",
    name: "withCharts",
    message: "O seu projeto vai usar gráficos? (biblioteca @zoox-ui/charts)",
    default: false,
  },
  {
    type: "confirm",
    name: "withMobile",
    message: "O seu é responsivo e precisa de layout de celular?",
    default: false,
  },
];
